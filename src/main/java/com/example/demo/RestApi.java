package com.example.demo;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/api")
@ResponseBody
public class RestApi {

    @RequestMapping(value = "say")
    public ResponseEntity onReportRequest(@RequestParam(value = "name") String name){
        System.out.println("Hello "+name+" !!!");
        return ResponseEntity.ok("Hello "+name+" !!!");
    }

}
